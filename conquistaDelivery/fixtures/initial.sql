-- MySQL dump 10.13  Distrib 5.5.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: delivery
-- ------------------------------------------------------
-- Server version	5.5.33-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add tipo produto',7,'add_tipoproduto'),(20,'Can change tipo produto',7,'change_tipoproduto'),(21,'Can delete tipo produto',7,'delete_tipoproduto'),(22,'Can add produto',8,'add_produto'),(23,'Can change produto',8,'change_produto'),(24,'Can delete produto',8,'delete_produto'),(25,'Can add estabelecimento',9,'add_estabelecimento'),(26,'Can change estabelecimento',9,'change_estabelecimento'),(27,'Can delete estabelecimento',9,'delete_estabelecimento'),(28,'Can add endereco',10,'add_endereco'),(29,'Can change endereco',10,'change_endereco'),(30,'Can delete endereco',10,'delete_endereco'),(31,'Can add contato',11,'add_contato'),(32,'Can change contato',11,'change_contato'),(33,'Can delete contato',11,'delete_contato'),(34,'Can add horario',12,'add_horario'),(35,'Can change horario',12,'change_horario'),(36,'Can delete horario',12,'delete_horario'),(37,'Can add tipo cozinha',13,'add_tipocozinha'),(38,'Can change tipo cozinha',13,'change_tipocozinha'),(39,'Can delete tipo cozinha',13,'delete_tipocozinha');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$vmA9ktBVWyRo$EAL9TAiqqj9sunVMlOPApgMkxdeUNIC7V6eLL/TaE5w=','2014-06-19 14:44:44',1,'delivery','','','',1,1,'2014-06-15 18:10:27');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_6340c63c` (`user_id`),
  KEY `auth_user_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_274b862c` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `user_id_refs_id_40c41112` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_6340c63c` (`user_id`),
  KEY `auth_user_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `permission_id_refs_id_35d9ac25` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `user_id_refs_id_4dc23c39` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_c0d12874` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2014-06-17 00:06:16',1,7,'51','Ingrediente',1,''),(2,'2014-06-17 00:07:27',1,8,'1','Pão',1,''),(3,'2014-06-17 00:08:05',1,8,'2','hamburguer',1,''),(4,'2014-06-17 03:43:35',1,8,'83','molho tarê',2,'Modificado nome.'),(5,'2014-06-17 03:43:45',1,8,'80','camarão grelhado',2,'Modificado nome.'),(6,'2014-06-17 03:44:05',1,8,'76','molho tarê com massa de rolinho',2,'Modificado nome.'),(7,'2014-06-17 03:44:17',1,8,'74','salmão enrolado por fora',2,'Modificado nome.'),(8,'2014-06-17 03:44:28',1,8,'78','salmão picado',2,'Modificado nome.'),(9,'2014-06-17 03:44:43',1,8,'70','salmão',2,'Modificado nome.'),(10,'2014-06-17 03:44:58',1,8,'69','camarão empanado',2,'Modificado nome.'),(11,'2014-06-17 03:45:19',1,8,'10','orégano',2,'Modificado nome.'),(12,'2014-06-17 03:45:40',1,8,'67','pele de salmão grelhada',2,'Modificado nome.'),(13,'2014-06-17 03:45:59',1,8,'65','salmão grelhado',2,'Modificado nome.'),(14,'2014-06-17 03:46:11',1,8,'54','pão de forma',2,'Modificado nome.'),(15,'2014-06-17 03:46:25',1,8,'58','filé de frango',2,'Modificado nome.'),(16,'2014-06-17 03:46:46',1,8,'46','coração de frango',2,'Modificado nome.'),(17,'2014-06-17 03:47:42',1,8,'51','2 hambúrgueres',2,'Modificado nome.'),(18,'2014-06-17 03:47:55',1,8,'45','pão francês',2,'Modificado nome.'),(19,'2014-06-17 03:48:03',1,8,'44','molho inglês',2,'Modificado nome.'),(20,'2014-06-17 03:48:25',1,8,'40','filé mignhom picado',2,'Modificado nome.'),(21,'2014-06-17 03:48:35',1,8,'33','pimentão',2,'Modificado nome.'),(22,'2014-06-17 03:48:42',1,8,'29','manjericão',2,'Modificado nome.'),(23,'2014-06-17 03:48:49',1,8,'26','camarão',2,'Modificado nome.'),(24,'2014-06-17 03:48:56',1,8,'12','parmesão',2,'Modificado nome.'),(25,'2014-06-17 23:11:44',1,13,'1','Comida Congelada',1,''),(26,'2014-06-17 23:12:04',1,13,'2','Pizzaria',1,''),(27,'2014-06-17 23:12:21',1,13,'3','Chinesa',1,''),(28,'2014-06-17 23:12:42',1,13,'4','Japonesa',1,''),(29,'2014-06-17 23:13:18',1,13,'5','Lanches',1,''),(30,'2014-06-18 23:17:38',1,10,'1','Av. Brasil, 750, Candeias',1,''),(31,'2014-06-18 23:18:53',1,10,'2','Rua Paulo Filadelfo, 45, Candeias',1,''),(32,'2014-06-18 23:19:47',1,10,'3','Av. Barreiras, 2407, Bairro Brasil',1,''),(33,'2014-06-18 23:21:08',1,10,'4','Av. Boa Vontade,    , Bairro Brasil',1,''),(34,'2014-06-19 00:03:11',1,12,'5','2 a 6 de 18:00 às 00:00',1,''),(35,'2014-06-19 00:03:38',1,12,'6','2º a 6º de 18:00 às 00:00',1,''),(36,'2014-06-19 00:03:53',1,12,'7','2º a 6º de 18:00 às 00:00',1,''),(37,'2014-06-19 00:04:01',1,12,'8','2º a 6º de 18:00 às 00:00',1,''),(38,'2014-06-19 00:04:17',1,12,'5','2º a 6º de 18:00 às 00:00',2,'Modificado dia.'),(39,'2014-06-19 00:06:38',1,11,'1','(77)3420-8096',1,''),(40,'2014-06-19 00:07:14',1,11,'2','(77)3421-1233 /3421-1250',1,''),(41,'2014-06-19 00:07:45',1,11,'3','(77)3424-3496',1,''),(42,'2014-06-19 00:08:41',1,11,'4','(77)34217768 / 8856-4258',1,''),(43,'2014-06-19 14:46:38',1,8,'84','Escondidinho de bacalhau',1,''),(44,'2014-06-19 14:47:26',1,8,'85','Escondidinho de carne do sol',1,''),(45,'2014-06-19 14:48:08',1,8,'86','Escondidinho de calabresa com banana',1,''),(46,'2014-06-19 14:48:38',1,8,'87','Escondidinho de carne de fumeiro',1,''),(47,'2014-06-19 14:49:15',1,8,'88','Escondidinho de camarao',1,''),(48,'2014-06-19 14:49:51',1,8,'89','Escondidinho de carne seca com catupiry',1,''),(49,'2014-06-19 14:50:26',1,8,'90','Escondidinho de Frango',1,''),(50,'2014-06-19 14:51:04',1,8,'91','Lasanha de Frango',1,''),(51,'2014-06-19 14:51:37',1,8,'92','Lasanha à Bolonhesa',1,''),(52,'2014-06-19 14:52:43',1,8,'93','Filé de carne à parmegiana / purê de batata',1,''),(53,'2014-06-19 14:53:20',1,8,'94','Filézinho ao molho gorgonzola',1,''),(54,'2014-06-19 14:53:44',1,8,'95','Filé ao molho madeira',1,''),(55,'2014-06-19 14:54:44',1,8,'96','Filé de frango à parmegiana c/ arroz branco',1,''),(56,'2014-06-19 14:55:17',1,8,'97','Filé de frango ao molho de mostarda',1,''),(57,'2014-06-19 14:55:47',1,8,'98','Filé de frango ao creme de milho',1,''),(58,'2014-06-19 14:56:14',1,8,'99','Frango Xadrex',1,''),(59,'2014-06-19 14:56:55',1,8,'100','Strogonoff de carne c/ arroz branco',1,''),(60,'2014-06-19 14:57:37',1,8,'101','Strogonoff de frango c/ arroz branco',1,''),(61,'2014-06-19 14:57:48',1,8,'100','Strogonoff de carne c/ arroz branco',2,'Modificado preco.'),(62,'2014-06-19 14:58:11',1,8,'102','Sarapatel',1,''),(63,'2014-06-19 14:58:52',1,8,'103','Panqueca de Carne',1,''),(64,'2014-06-19 14:59:39',1,8,'104','Panqueca de Frango',1,''),(65,'2014-06-19 15:00:18',1,8,'105','Panqueca de 3 queijos',1,''),(66,'2014-06-19 15:00:49',1,8,'106','Feijoada',1,''),(67,'2014-06-19 15:01:15',1,8,'107','Arroz Carreteiro',1,''),(68,'2014-06-19 15:01:51',1,8,'108','Fricassê de Frango',1,''),(69,'2014-06-19 15:02:21',1,8,'109','Moqueca de Abadejo',1,''),(70,'2014-06-19 15:03:39',1,8,'110','Arroz à grega',1,''),(71,'2014-06-19 15:04:19',1,8,'111','Arroz com Milho',1,''),(72,'2014-06-19 15:04:48',1,8,'112','Arroz Integral',1,''),(73,'2014-06-19 15:05:23',1,8,'113','Arroz Branco',1,''),(74,'2014-06-19 15:06:01',1,8,'114','Arroz com Brócolis',1,''),(75,'2014-06-19 15:06:34',1,8,'115','Purê de Batata',1,''),(76,'2014-06-19 15:07:08',1,8,'116','Fejão Verde',1,''),(77,'2014-06-19 15:09:08',1,8,'117','Mini pizza - 4 unidades',1,''),(78,'2014-06-19 15:10:28',1,8,'118','Pão de queijo - 12 unidades',1,''),(79,'2014-06-19 15:10:57',1,8,'119','Caldo de Frango',1,''),(80,'2014-06-19 15:11:52',1,8,'120','Caldo de Vaca Atolada',1,''),(81,'2014-06-19 15:12:58',1,8,'121','Caldo Verde',1,''),(82,'2014-06-19 15:13:19',1,8,'122','Caldo de Feijão',1,''),(83,'2014-06-19 15:14:39',1,8,'123','Salmão empanado com gergelin',1,''),(84,'2014-06-19 15:15:28',1,8,'124','Panqueca integral de ricota e espinafre',1,''),(85,'2014-06-19 15:17:00',1,8,'125','Strogonoff de frango c/ arroz integral',1,''),(86,'2014-06-19 15:17:24',1,8,'126','Lasanha de Beringela',1,''),(87,'2014-06-19 15:18:01',1,8,'127','Pavê de Morango',1,''),(88,'2014-06-19 15:19:01',1,8,'128','Pavê de Pêssego',1,''),(89,'2014-06-19 15:19:22',1,8,'129','Pavê de Abacaxi',1,''),(90,'2014-06-19 15:19:51',1,8,'130','Pavê de Chocolate',1,''),(91,'2014-06-19 15:20:31',1,8,'131','Bolinho de Bacalhau - 12 unidades',1,''),(92,'2014-06-19 15:21:13',1,8,'132','Bolinho de mandioca recheado c/ carne de sol - 12 unidades',1,''),(93,'2014-06-19 15:21:53',1,8,'133','Kibe - 12 unidades',1,''),(94,'2014-06-19 15:22:34',1,8,'134','Coxinha de frango com catupiry - 12 unidades ',1,''),(95,'2014-06-19 16:18:11',1,8,'90','Escondidinho de Frango',2,'Modificado preco.'),(96,'2014-06-19 16:18:26',1,8,'89','Escondidinho de carne seca com catupiry',2,'Modificado preco.'),(97,'2014-06-19 16:18:39',1,8,'88','Escondidinho de camarao',2,'Modificado preco.'),(98,'2014-06-19 16:18:53',1,8,'87','Escondidinho de carne de fumeiro',2,'Modificado preco.'),(99,'2014-06-19 16:19:19',1,8,'86','Escondidinho de calabresa com banana',2,'Modificado preco.'),(100,'2014-06-19 16:19:38',1,8,'85','Escondidinho de carne do sol',2,'Modificado preco.'),(101,'2014-06-19 16:19:52',1,8,'92','Lasanha à Bolonhesa',2,'Modificado preco.'),(102,'2014-06-19 16:20:01',1,8,'91','Lasanha de Frango',2,'Modificado preco.'),(103,'2014-06-19 16:20:29',1,8,'93','Filé de carne à parmegiana c/ purê de batata',2,'Modificado nome e preco.'),(104,'2014-06-19 16:21:50',1,8,'94','Filézinho ao molho gorgonzola',2,'Modificado preco.'),(105,'2014-06-19 16:21:58',1,8,'95','Filé ao molho madeira',2,'Modificado preco.'),(106,'2014-06-19 16:22:12',1,8,'96','Filé de frango à parmegiana c/ arroz branco',2,'Modificado preco.'),(107,'2014-06-19 16:22:23',1,8,'97','Filé de frango ao molho de mostarda',2,'Modificado preco.'),(108,'2014-06-19 16:22:35',1,8,'98','Filé de frango ao creme de milho',2,'Modificado preco.'),(109,'2014-06-19 16:22:45',1,8,'99','Frango Xadrex',2,'Modificado preco.'),(110,'2014-06-19 16:22:59',1,8,'84','Escondidinho de bacalhau',2,'Modificado preco.'),(111,'2014-06-19 16:33:03',1,8,'135','Batata Frita - P',1,''),(112,'2014-06-19 16:33:30',1,8,'136','Batata Frita - G',1,''),(113,'2014-06-19 16:34:15',1,8,'137','Mini Coxinhas - 10 unidades',1,''),(114,'2014-06-19 16:34:49',1,8,'138','Mini Kibe - 10 unidades',1,''),(115,'2014-06-19 16:35:36',1,8,'139','Mini Kibes e Coxinhas - 10 unidades',1,''),(116,'2014-06-19 16:36:05',1,8,'140','Mini Bolinho de bacalhau - 10 unidades',1,''),(117,'2014-06-19 16:37:55',1,8,'141','Água Mineral - 500ml',1,''),(118,'2014-06-19 16:38:41',1,8,'142','Água Mineral c/ gás- 500ml',1,''),(119,'2014-06-19 16:39:29',1,8,'143','Cerveja - 350 e 473ml ',1,''),(120,'2014-06-19 16:40:12',1,8,'144','Refrigerante - 350 e 600ml',1,''),(121,'2014-06-19 16:40:35',1,8,'144','Refrigerante - 350, 600ml e 1l',2,'Modificado nome.'),(122,'2014-06-19 16:41:18',1,8,'145','Aquarius Fresh - 510ml',1,''),(123,'2014-06-19 16:41:47',1,8,'146','Red Bull - 250ml',1,''),(124,'2014-06-19 16:42:28',1,8,'147','acerola',1,''),(125,'2014-06-19 16:43:25',1,8,'148','goiaba',1,''),(126,'2014-06-19 16:43:38',1,8,'149','manga',1,''),(127,'2014-06-19 16:43:54',1,8,'150','cacau',1,''),(128,'2014-06-19 16:44:26',1,8,'151','cajá',1,''),(129,'2014-06-19 16:44:40',1,8,'152','caju',1,''),(130,'2014-06-19 16:44:58',1,8,'153','umbu',1,''),(131,'2014-06-19 16:45:20',1,8,'154','gravilola',1,''),(132,'2014-06-19 16:45:38',1,8,'155','morango',1,''),(133,'2014-06-19 16:45:57',1,8,'156','maracujá',1,''),(134,'2014-06-19 16:46:16',1,8,'157','açai',1,''),(135,'2014-06-19 16:46:33',1,8,'158','açai potente',1,''),(136,'2014-06-19 16:47:39',1,8,'159','Normal',1,''),(137,'2014-06-19 16:56:26',1,8,'160','Com Leite',1,''),(138,'2014-06-19 16:59:22',1,8,'160','Com Leite',2,'Nenhum campo modificado.'),(139,'2014-06-19 16:59:37',1,8,'160','Com Leite',2,'Modificado componentes.'),(140,'2014-06-19 16:59:43',1,8,'160','Com Leite',3,''),(141,'2014-06-19 16:59:54',1,8,'159','Normal',2,'Modificado componentes.'),(142,'2014-06-19 16:59:58',1,8,'159','Normal',3,''),(143,'2014-06-19 17:00:33',1,8,'161','Acerola',1,''),(144,'2014-06-19 17:01:06',1,8,'162','Acerola c/ Leite',1,''),(145,'2014-06-19 17:01:40',1,8,'163','Goiaba',1,''),(146,'2014-06-19 17:02:08',1,8,'164','Goiaba c/ Leite',1,''),(147,'2014-06-19 17:02:25',1,8,'165','Manga',1,''),(148,'2014-06-19 17:03:17',1,8,'166','Manga c/ Leite',1,''),(149,'2014-06-19 17:03:34',1,8,'167','Cacau',1,''),(150,'2014-06-19 17:03:55',1,8,'168','Cacau c/ Leite',1,''),(151,'2014-06-19 17:04:22',1,8,'169','Cajá',1,''),(152,'2014-06-19 17:04:44',1,8,'170','Cajá c/ Leite',1,''),(153,'2014-06-19 17:05:05',1,8,'171','Caju',1,''),(154,'2014-06-19 17:05:23',1,8,'172','Caju c/ Leite',1,''),(155,'2014-06-19 17:05:52',1,8,'173','Umbu',1,''),(156,'2014-06-19 17:06:18',1,8,'174','Umbu c/ Leite',1,''),(157,'2014-06-19 17:06:36',1,8,'175','Graviola',1,''),(158,'2014-06-19 17:07:03',1,8,'176','Graviola c/ Leite',1,''),(159,'2014-06-19 17:07:27',1,8,'177','Morango',1,''),(160,'2014-06-19 17:08:02',1,8,'178','Morango c/ Leite',1,''),(161,'2014-06-19 17:08:20',1,8,'179','Maracujá',1,''),(162,'2014-06-19 17:08:44',1,8,'180','Maracujá c/ Leite',1,''),(163,'2014-06-19 17:09:10',1,8,'181','Açai Potente',1,''),(164,'2014-06-19 17:10:10',1,8,'182','Açai Potente c/ Leite',1,'');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'tipo produto','produtos','tipoproduto'),(8,'produto','produtos','produto'),(9,'estabelecimento','estabelecimentos','estabelecimento'),(10,'endereco','estabelecimentos','endereco'),(11,'contato','estabelecimentos','contato'),(12,'horario','estabelecimentos','horario'),(13,'tipo cozinha','estabelecimentos','tipocozinha');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('ej8u6mrw48jzm6zftv2dl4vm6e3alw6x','NDNlZDI4NTA3ZjNhNWNkY2QxM2UzNTRhMjU4YjY1MzUyZDI3OWQzNDp7fQ==','2014-07-01 03:50:35'),('i5de9tua7xrbjtg6rjzq1upsxp2zchly','ZDgxMjU1MjU0ZGNhN2JmMzQ3NTc4MmZkZmEzMzBiNzg4ZTRlZjhmNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-07-01 23:10:49'),('t73z00am2a4vlgilg3ut0xqwl0e08qg4','ZDgxMjU1MjU0ZGNhN2JmMzQ3NTc4MmZkZmEzMzBiNzg4ZTRlZjhmNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6MX0=','2014-07-03 14:44:44');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estabelecimentos_contato`
--

DROP TABLE IF EXISTS `estabelecimentos_contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estabelecimentos_contato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) NOT NULL,
  `contato` varchar(255) NOT NULL,
  `estabelecimento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estabelecimentos_contato_6d15b8dd` (`estabelecimento_id`),
  CONSTRAINT `estabelecimento_id_refs_id_f92ed994` FOREIGN KEY (`estabelecimento_id`) REFERENCES `estabelecimentos_estabelecimento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estabelecimentos_contato`
--

LOCK TABLES `estabelecimentos_contato` WRITE;
/*!40000 ALTER TABLE `estabelecimentos_contato` DISABLE KEYS */;
INSERT INTO `estabelecimentos_contato` VALUES (1,'tel','(77)3420-8096',3),(2,'tel','(77)3421-1233 /3421-1250',1),(3,'tel','(77)3424-3496',2),(4,'tel','(77)34217768 / 8856-4258',4);
/*!40000 ALTER TABLE `estabelecimentos_contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estabelecimentos_endereco`
--

DROP TABLE IF EXISTS `estabelecimentos_endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estabelecimentos_endereco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rua` varchar(255) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `numero` varchar(255) NOT NULL,
  `complemento` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `estabelecimento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estabelecimentos_endereco_6d15b8dd` (`estabelecimento_id`),
  CONSTRAINT `estabelecimento_id_refs_id_67203fc2` FOREIGN KEY (`estabelecimento_id`) REFERENCES `estabelecimentos_estabelecimento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estabelecimentos_endereco`
--

LOCK TABLES `estabelecimentos_endereco` WRITE;
/*!40000 ALTER TABLE `estabelecimentos_endereco` DISABLE KEYS */;
INSERT INTO `estabelecimentos_endereco` VALUES (1,'Av. Brasil','Candeias','750','B','0','0',3),(2,'Rua Paulo Filadelfo','Candeias','45','C','0','0',2),(3,'Av. Barreiras','Bairro Brasil','2407','   ','0','0',4),(4,'Av. Boa Vontade','Bairro Brasil','   ','Esquina com a Brumado','0','0',1);
/*!40000 ALTER TABLE `estabelecimentos_endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estabelecimentos_estabelecimento`
--

DROP TABLE IF EXISTS `estabelecimentos_estabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estabelecimentos_estabelecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `logotipo` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `capa` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estabelecimentos_estabelecimento_f52cfca0` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estabelecimentos_estabelecimento`
--

LOCK TABLES `estabelecimentos_estabelecimento` WRITE;
/*!40000 ALTER TABLE `estabelecimentos_estabelecimento` DISABLE KEYS */;
INSERT INTO `estabelecimentos_estabelecimento` VALUES (1,'O Gostosão','Lanches e Fritas','o-gostosao','   ','   '),(2,'Congelart','Pratos Congelados','congelart','   ','   '),(3,'ChinaÊ','Culinária Oriental','chinae','   ','   '),(4,'Ponto da Pizza','   ','ponto-da-pizza','   ','   ');
/*!40000 ALTER TABLE `estabelecimentos_estabelecimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estabelecimentos_horario`
--

DROP TABLE IF EXISTS `estabelecimentos_horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estabelecimentos_horario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dia` varchar(255) NOT NULL,
  `abertura` varchar(255) NOT NULL,
  `encerramento` varchar(255) NOT NULL,
  `estabelecimento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estabelecimentos_horario_6d15b8dd` (`estabelecimento_id`),
  CONSTRAINT `estabelecimento_id_refs_id_9ed9ca93` FOREIGN KEY (`estabelecimento_id`) REFERENCES `estabelecimentos_estabelecimento` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estabelecimentos_horario`
--

LOCK TABLES `estabelecimentos_horario` WRITE;
/*!40000 ALTER TABLE `estabelecimentos_horario` DISABLE KEYS */;
INSERT INTO `estabelecimentos_horario` VALUES (5,'2º a 6º','18:00','00:00',1),(6,'2º a 6º','18:00','00:00',2),(7,'2º a 6º','18:00','00:00',3),(8,'2º a 6º','18:00','00:00',4);
/*!40000 ALTER TABLE `estabelecimentos_horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estabelecimentos_tipocozinha`
--

DROP TABLE IF EXISTS `estabelecimentos_tipocozinha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estabelecimentos_tipocozinha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estabelecimentos_tipocozinha_f52cfca0` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estabelecimentos_tipocozinha`
--

LOCK TABLES `estabelecimentos_tipocozinha` WRITE;
/*!40000 ALTER TABLE `estabelecimentos_tipocozinha` DISABLE KEYS */;
INSERT INTO `estabelecimentos_tipocozinha` VALUES (1,'Comida Congelada','comida-congelada'),(2,'Pizzaria','pizzaria'),(3,'Chinesa','chinesa'),(4,'Japonesa','japonesa'),(5,'Lanches','lanches');
/*!40000 ALTER TABLE `estabelecimentos_tipocozinha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estabelecimentos_tipocozinha_stabelecimento`
--

DROP TABLE IF EXISTS `estabelecimentos_tipocozinha_stabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estabelecimentos_tipocozinha_stabelecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipocozinha_id` int(11) NOT NULL,
  `estabelecimento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipocozinha_id` (`tipocozinha_id`,`estabelecimento_id`),
  KEY `estabelecimentos_tipocozinha_stabelecimento_75605a0f` (`tipocozinha_id`),
  KEY `estabelecimentos_tipocozinha_stabelecimento_6d15b8dd` (`estabelecimento_id`),
  CONSTRAINT `estabelecimento_id_refs_id_162d6e6c` FOREIGN KEY (`estabelecimento_id`) REFERENCES `estabelecimentos_estabelecimento` (`id`),
  CONSTRAINT `tipocozinha_id_refs_id_3b67a5fb` FOREIGN KEY (`tipocozinha_id`) REFERENCES `estabelecimentos_tipocozinha` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estabelecimentos_tipocozinha_stabelecimento`
--

LOCK TABLES `estabelecimentos_tipocozinha_stabelecimento` WRITE;
/*!40000 ALTER TABLE `estabelecimentos_tipocozinha_stabelecimento` DISABLE KEYS */;
INSERT INTO `estabelecimentos_tipocozinha_stabelecimento` VALUES (1,1,2),(2,2,4),(3,3,3),(4,4,3),(5,5,1);
/*!40000 ALTER TABLE `estabelecimentos_tipocozinha_stabelecimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_produto`
--

DROP TABLE IF EXISTS `produtos_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `preco` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `estabelecimento_id` int(11) DEFAULT NULL,
  `tipoProduto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produtos_produto_f52cfca0` (`slug`),
  KEY `produtos_produto_6d15b8dd` (`estabelecimento_id`),
  KEY `produtos_produto_ab61bd48` (`tipoProduto_id`),
  CONSTRAINT `estabelecimento_id_refs_id_a99adf5e` FOREIGN KEY (`estabelecimento_id`) REFERENCES `estabelecimentos_estabelecimento` (`id`),
  CONSTRAINT `tipoProduto_id_refs_id_1ce59218` FOREIGN KEY (`tipoProduto_id`) REFERENCES `produtos_tipoproduto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_produto`
--

LOCK TABLES `produtos_produto` WRITE;
/*!40000 ALTER TABLE `produtos_produto` DISABLE KEYS */;
INSERT INTO `produtos_produto` VALUES (1,'Pão','pao','','',NULL,51),(2,'hamburguer','hamburguer','','',NULL,51),(5,'bacon','bacon','','',NULL,51),(6,'queijo','queijo','','',NULL,51),(7,'molho','molho','','',NULL,51),(8,'calabresa','calabresa','','',NULL,51),(9,'mussarela','mussarela','','',NULL,51),(10,'orégano','oregano','','',NULL,51),(11,'azeitona','azeitona','','',NULL,51),(12,'parmesão','parmesao','','',NULL,51),(13,'tomate','tomate','','',NULL,51),(14,'frango','frango','','',NULL,51),(15,'presunto','presunto','','',NULL,51),(16,'milho\n','milho','','',NULL,51),(17,'atum','atum','','',NULL,51),(18,'palmito','palmito','','',NULL,51),(19,'cebola','cebola','','',NULL,51),(20,'cheiro verde','cheiro-verde','','',NULL,51),(21,'calabresa moida','calabresa-moida','','',NULL,51),(22,'ovo','ovo','','',NULL,51),(23,'pimenta','pimenta','','',NULL,51),(24,'champignon','champignon','','',NULL,51),(25,'catupiry','catupiry','','',NULL,51),(26,'camarão','camarao','','',NULL,51),(27,'bacalhau\n','bacalhau','','',NULL,51),(28,'lombo','lombo','','',NULL,51),(29,'manjericão','manjericao','','',NULL,51),(30,'peito de peru','peito-de-peru','','',NULL,51),(31,'ervilha','ervilha','','',NULL,51),(32,'carne seca','carne-seca','','',NULL,51),(33,'pimentão','pimentao','','',NULL,51),(34,'alface','alface','','',NULL,51),(35,'rosbife','rosbife','','',NULL,51),(36,'peito de peru prensado\n','peito-de-peru-prensado','','',NULL,51),(37,'cheddar','cheddar','','',NULL,51),(38,'recheio de cheddar','recheio-de-cheddar','','',NULL,51),(39,'recheio de catupiry','recheio-de-catupiry','','',NULL,51),(40,'filé mignhom picado','file-mignhom-picado','','',NULL,51),(41,'frango desfiado','frango-desfiado','','',NULL,51),(42,'salsicha','salsicha','','',NULL,51),(43,'milho verde\n','milho-verde','','',NULL,51),(44,'molho inglês','molho-ingles','','',NULL,51),(45,'pão francês','pao-frances','','',NULL,51),(46,'coração de frango','coracao-de-frango','','',NULL,51),(47,'batata palha','batata-palha','','',NULL,51),(48,'catchup','catchup','','',NULL,51),(49,'maionese','maionese','','',NULL,51),(50,'2 salsichas','2-salsichas','','',NULL,51),(51,'2 hambúrgueres','2-hamburgueres','','',NULL,51),(52,'2 presuntos','2-presuntos','','',NULL,51),(53,'2 queijos','2-queijos','','',NULL,51),(54,'pão de forma','pao-de-forma','','',NULL,51),(55,'peito de peru defumado','peito-de-peru-defumado','','',NULL,51),(56,'molho de pimenta','molho-de-pimenta','','',NULL,51),(57,' presunto picado','presunto-picado','','',NULL,51),(58,'filé de frango','file-de-frango','','',NULL,51),(59,'tomate picado','tomate-picado','','',NULL,51),(60,'arroz','arroz','','',NULL,51),(61,'nori','nori','','',NULL,51),(62,'manga','manga','','',NULL,51),(63,'cany','cany','','',NULL,51),(64,'pepino','pepino','','',NULL,51),(65,'salmão grelhado','salmao-grelhado','','',NULL,51),(66,'creme chease','creme-chease','','',NULL,51),(67,'pele de salmão grelhada','pele-de-salmao-grelhada','','',NULL,51),(68,'cebolinha','cebolinha','','',NULL,51),(69,'camarão empanado','camarao-empanado','','',NULL,51),(70,'salmão','salmao','','',NULL,51),(71,'kanikama','kanikama','','',NULL,51),(72,'nori empanado','nori-empanado','','',NULL,51),(73,'kani','kani','','',NULL,51),(74,'salmão enrolado por fora','salmao-enrolado-por-fora','','',NULL,51),(75,'empanado com molho teriaki\n','empanado-com-molho-teriaki','','',NULL,51),(76,'molho tarê com massa de rolinho','molho-tare-com-massa-de-rolinho','','',NULL,51),(77,'empanado e frito','empanado-e-frito','','',NULL,51),(78,'salmão picado','salmao-picado','','',NULL,51),(79,'gergilim','gergilim','','',NULL,51),(80,'camarão grelhado','camarao-grelhado','','',NULL,51),(81,'massa de haromaki frito\n','massa-de-haromaki-frito','','',NULL,51),(82,'sushi prensado com arroz','sushi-prensado-com-arroz','','',NULL,51),(83,'molho tarê','molho-tare','','',NULL,51),(84,'Escondidinho de bacalhau','escondidinho-de-bacalhau','30,00','',2,7),(85,'Escondidinho de carne do sol','escondidinho-de-carne-do-sol','20,00','',2,7),(86,'Escondidinho de calabresa com banana','escondidinho-de-calabresa-com-banana','17,00','',2,7),(87,'Escondidinho de carne de fumeiro','escondidinho-de-carne-de-fumeiro','24,00','',2,7),(88,'Escondidinho de camarao','escondidinho-de-camarao','36,00','',2,7),(89,'Escondidinho de carne seca com catupiry','escondidinho-de-carne-seca-com-catupiry','20,00','',2,7),(90,'Escondidinho de Frango','escondidinho-de-frango','18,00','',2,7),(91,'Lasanha de Frango','lasanha-de-frango','18,00','',2,7),(92,'Lasanha à Bolonhesa','lasanha-a-bolonhesa','18,00','',2,7),(93,'Filé de carne à parmegiana c/ purê de batata','file-de-carne-a-parmegiana-c-pure-de-batata','26,00','',2,7),(94,'Filézinho ao molho gorgonzola','filezinho-ao-molho-gorgonzola','30,00','',2,7),(95,'Filé ao molho madeira','file-ao-molho-madeira','24,00','',2,7),(96,'Filé de frango à parmegiana c/ arroz branco','file-de-frango-a-parmegiana-c-arroz-branco','18,00','',2,7),(97,'Filé de frango ao molho de mostarda','file-de-frango-ao-molho-de-mostarda','16,00','',2,7),(98,'Filé de frango ao creme de milho','file-de-frango-ao-creme-de-milho','16,00','',2,7),(99,'Frango Xadrex','frango-xadrex','18,00','',2,7),(100,'Strogonoff de carne c/ arroz branco','strogonoff-de-carne-c-arroz-branco','20,00','',2,7),(101,'Strogonoff de frango c/ arroz branco','strogonoff-de-frango-c-arroz-branco','18,00','',2,7),(102,'Sarapatel','sarapatel','13,00','',2,7),(103,'Panqueca de Carne','panqueca-de-carne','16,00','',2,7),(104,'Panqueca de Frango','panqueca-de-frango','16,00','',2,7),(105,'Panqueca de 3 queijos','panqueca-de-3-queijos','16,00','',2,7),(106,'Feijoada','feijoada','20,00','',2,7),(107,'Arroz Carreteiro','arroz-carreteiro','15,00','',2,7),(108,'Fricassê de Frango','fricasse-de-frango','18,00','',2,7),(109,'Moqueca de Abadejo','moqueca-de-abadejo','22,00','',2,7),(110,'Arroz à grega','arroz-a-grega','2,80','',2,8),(111,'Arroz com Milho','arroz-com-milho','2,80','',2,8),(112,'Arroz Integral','arroz-integral','2,50','',2,8),(113,'Arroz Branco','arroz-branco','2,50','',2,8),(114,'Arroz com Brócolis','arroz-com-brocolis','2,80','',2,8),(115,'Purê de Batata','pure-de-batata','2,70','',2,8),(116,'Fejão Verde','fejao-verde','4,00','',2,8),(117,'Mini pizza - 4 unidades','mini-pizza-4-unidades','6,50','',2,9),(118,'Pão de queijo - 12 unidades','pao-de-queijo-12-unidades','6,50','',2,9),(119,'Caldo de Frango','caldo-de-frango','7,50','',2,50),(120,'Caldo de Vaca Atolada','caldo-de-vaca-atolada','7,50','',2,50),(121,'Caldo Verde','caldo-verde','7,50','',2,50),(122,'Caldo de Feijão','caldo-de-feijao','7,50','',2,50),(123,'Salmão empanado com gergelin','salmao-empanado-com-gergelin','25,00','',2,10),(124,'Panqueca integral de ricota e espinafre','panqueca-integral-de-ricota-e-espinafre','18,00','',2,10),(125,'Strogonoff de frango c/ arroz integral','strogonoff-de-frango-c-arroz-integral','20,00','',2,10),(126,'Lasanha de Beringela','lasanha-de-beringela','16,00','',2,10),(127,'Pavê de Morango','pave-de-morango','7,50','',2,11),(128,'Pavê de Pêssego','pave-de-pessego','7,50','',2,11),(129,'Pavê de Abacaxi','pave-de-abacaxi','7,50','',2,11),(130,'Pavê de Chocolate','pave-de-chocolate','7,50','',2,11),(131,'Bolinho de Bacalhau - 12 unidades','bolinho-de-bacalhau-12-unidades','10,00','',2,12),(132,'Bolinho de mandioca recheado c/ carne de sol - 12 unidades','bolinho-de-mandioca-recheado-c-carne-de-sol-12-unidades','9,00','',2,12),(133,'Kibe - 12 unidades','kibe-12-unidades','9,00','',2,12),(134,'Coxinha de frango com catupiry - 12 unidades ','coxinha-de-frango-com-catupiry-12-unidades','9,00','',2,12),(135,'Batata Frita - P','batata-frita-p','4,80','',1,4),(136,'Batata Frita - G','batata-frita-g','7,90','',1,4),(137,'Mini Coxinhas - 10 unidades','mini-coxinhas-10-unidades','9,00','',1,4),(138,'Mini Kibe - 10 unidades','mini-kibe-10-unidades','9,00','',1,4),(139,'Mini Kibes e Coxinhas - 10 unidades','mini-kibes-e-coxinhas-10-unidades','9,00','',1,4),(140,'Mini Bolinho de bacalhau - 10 unidades','mini-bolinho-de-bacalhau-10-unidades','9,90','',1,4),(141,'Água Mineral - 500ml','agua-mineral-500ml','','',1,5),(142,'Água Mineral c/ gás- 500ml','agua-mineral-c-gas-500ml','','',1,5),(143,'Cerveja - 350 e 473ml ','cerveja-350-e-473ml','','',1,5),(144,'Refrigerante - 350, 600ml e 1l','refrigerante-350-600ml-e-1l','','',1,5),(145,'Aquarius Fresh - 510ml','aquarius-fresh-510ml','','',1,5),(146,'Red Bull - 250ml','red-bull-250ml','','',1,5),(147,'acerola','acerola','','',NULL,51),(148,'goiaba','goiaba','','',NULL,51),(149,'manga','manga','','',NULL,51),(150,'cacau','cacau','','',NULL,51),(151,'cajá','caja','','',NULL,51),(152,'caju','caju','','',NULL,51),(153,'umbu','umbu','','',NULL,51),(154,'gravilola','gravilola','','',NULL,51),(155,'morango','morango','','',NULL,51),(156,'maracujá','maracuja','','',NULL,51),(157,'açai','acai','','',NULL,51),(158,'açai potente','acai-potente','','',NULL,51),(161,'Acerola','acerola','2,50','',1,6),(162,'Acerola c/ Leite','acerola-c-leite','3,50','',1,6),(163,'Goiaba','goiaba','2,50','',1,6),(164,'Goiaba c/ Leite','goiaba-c-leite','3,50','',1,6),(165,'Manga','manga','2,50','',1,6),(166,'Manga c/ Leite','manga-c-leite','3,50','',1,6),(167,'Cacau','cacau','2,50','',1,6),(168,'Cacau c/ Leite','cacau-c-leite','3,50','',1,6),(169,'Cajá','caja','2,50','',1,6),(170,'Cajá c/ Leite','caja-c-leite','3,50','',1,6),(171,'Caju','caju','2,50','',1,6),(172,'Caju c/ Leite','caju-c-leite','3,50','',1,6),(173,'Umbu','umbu','3,50','',1,6),(174,'Umbu c/ Leite','umbu-c-leite','3,50','',1,6),(175,'Graviola','graviola','3,00','',1,6),(176,'Graviola c/ Leite','graviola-c-leite','4,00','',1,6),(177,'Morango','morango','3,00','',1,6),(178,'Morango c/ Leite','morango-c-leite','4,00','',1,6),(179,'Maracujá','maracuja','3,00','',1,6),(180,'Maracujá c/ Leite','maracuja-c-leite','4,00','',1,6),(181,'Açai Potente','acai-potente','3,50','',1,6),(182,'Açai Potente c/ Leite','acai-potente-c-leite','4,50','',1,6);
/*!40000 ALTER TABLE `produtos_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_produto_componentes`
--

DROP TABLE IF EXISTS `produtos_produto_componentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_produto_componentes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_produto_id` int(11) NOT NULL,
  `to_produto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `from_produto_id` (`from_produto_id`,`to_produto_id`),
  KEY `produtos_produto_componentes_63362ce0` (`from_produto_id`),
  KEY `produtos_produto_componentes_26c4c6fa` (`to_produto_id`),
  CONSTRAINT `from_produto_id_refs_id_f8f222f8` FOREIGN KEY (`from_produto_id`) REFERENCES `produtos_produto` (`id`),
  CONSTRAINT `to_produto_id_refs_id_f8f222f8` FOREIGN KEY (`to_produto_id`) REFERENCES `produtos_produto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_produto_componentes`
--

LOCK TABLES `produtos_produto_componentes` WRITE;
/*!40000 ALTER TABLE `produtos_produto_componentes` DISABLE KEYS */;
INSERT INTO `produtos_produto_componentes` VALUES (5,8,117),(6,9,117),(12,10,118),(7,14,117),(8,16,117),(13,38,118),(14,39,118),(1,117,8),(2,117,9),(3,117,14),(4,117,16),(9,118,10),(10,118,38),(11,118,39);
/*!40000 ALTER TABLE `produtos_produto_componentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_tipoproduto`
--

DROP TABLE IF EXISTS `produtos_tipoproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_tipoproduto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_tipoproduto`
--

LOCK TABLES `produtos_tipoproduto` WRITE;
/*!40000 ALTER TABLE `produtos_tipoproduto` DISABLE KEYS */;
INSERT INTO `produtos_tipoproduto` VALUES (1,'Lanches'),(2,'Lanches com sabor de Pizza'),(3,'Kit'),(4,'Porções'),(5,'Bebidas'),(6,'Sucos'),(7,'Pratos Prontos'),(8,'Acompanhamentos'),(9,'Fast Food'),(10,'Pratos Light'),(11,'Sobremessas'),(12,'Salgados'),(13,'Entradas'),(14,'Sobremesas'),(15,'Bebidas'),(16,'Uramaki'),(17,'Hossomaki'),(18,'Sushi Hot'),(19,'Temaki'),(20,'Shushis Especiais'),(21,'Sashimi'),(22,'Niguri'),(23,'Combinados'),(24,'Pizza'),(25,'Esfihas'),(26,'Beirutes'),(27,'Bebidas'),(50,'Caldos'),(51,'Ingrediente');
/*!40000 ALTER TABLE `produtos_tipoproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produtos_tipoproduto_estabelecimento`
--

DROP TABLE IF EXISTS `produtos_tipoproduto_estabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produtos_tipoproduto_estabelecimento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipoproduto_id` int(11) NOT NULL,
  `estabelecimento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tipoproduto_id` (`tipoproduto_id`,`estabelecimento_id`),
  KEY `produtos_tipoproduto_estabelecimento_7a18f8dd` (`tipoproduto_id`),
  KEY `produtos_tipoproduto_estabelecimento_6d15b8dd` (`estabelecimento_id`),
  CONSTRAINT `estabelecimento_id_refs_id_497ad6a1` FOREIGN KEY (`estabelecimento_id`) REFERENCES `estabelecimentos_estabelecimento` (`id`),
  CONSTRAINT `tipoproduto_id_refs_id_eeeb3dc5` FOREIGN KEY (`tipoproduto_id`) REFERENCES `produtos_tipoproduto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produtos_tipoproduto_estabelecimento`
--

LOCK TABLES `produtos_tipoproduto_estabelecimento` WRITE;
/*!40000 ALTER TABLE `produtos_tipoproduto_estabelecimento` DISABLE KEYS */;
INSERT INTO `produtos_tipoproduto_estabelecimento` VALUES (93,1,1),(94,2,1),(95,3,1),(96,4,1),(97,5,1),(98,6,1),(99,7,2),(100,8,2),(101,9,2),(102,10,2),(103,11,2),(104,12,2),(105,13,3),(106,14,3),(107,15,3),(108,16,3),(109,17,3),(110,18,3),(111,19,3),(112,20,3),(113,21,3),(114,22,3),(115,23,3),(116,24,4),(117,25,4),(118,26,4),(119,27,4),(120,50,2);
/*!40000 ALTER TABLE `produtos_tipoproduto_estabelecimento` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-21  9:22:37
