#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from django.contrib import admin
from .views import *
import estabelecimentos
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', home, name='home'),
    url(r'^search$', search, name='search'),
	url(r'^admin/', include(admin.site.urls)),
    url(r'^estabelecimentos', include('estabelecimentos.urls')),
    url(r'^cozinha/(?P<tipo_cozinha>[-\w]+)', estabelecimentos.views.cozinha, name="cozinha" ),
    url(r'^(?P<slug_estabelecimento>(?!admin)[-\w]+)', estabelecimentos.views.home, name="estabelecimentohome" ),
    # url(r'^blog/', include('blog.urls')),
)
