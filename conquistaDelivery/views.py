#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.db.models import Q
from django.template.defaultfilters import slugify
from django.views.decorators.csrf import csrf_exempt
from estabelecimentos.models import *
from produtos.models import *
import random
# Create your views here.

def home(request):
	index_img = random.randint(1,9)
	estabelecimentos = Estabelecimento.objects.all()
	cozinhas = TipoCozinha.objects.all()
	return render_to_response('conquistaDelivery/home.html', locals())

@csrf_exempt
def search(request):
	if request.method == 'POST':
		
		query = request.POST['search']
		produtos = Produto.objects.filter(
									(Q( slug__icontains=query)
									| Q( componentes__slug__icontains=query))
									& Q(estabelecimento__isnull=False)
								)
		if produtos.count() == 0:
			estabelecimentos = Estabelecimento.objects.filter( 
															Q( nome__icontains=query)
															| Q( enderecos__bairro__icontains=query)
															| Q( enderecos__rua__icontains=query)
															| Q( tiposProduto__nome__icontains=query)
															| Q( tiposCozinha__nome__icontains=query)
															)
			if estabelecimentos.count() == 0:
				estabelecimentos = Estabelecimento.objects.all()

			return render_to_response('estabelecimentos/index.html', locals() )
		return render_to_response('produtos/index.html', locals() )
		
	return render_to_response('conquistaDelivery/home.html', locals() )			