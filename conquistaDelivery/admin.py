#-*- coding: utf-8 -*-
from django.contrib import admin
from estabelecimentos.models import *
from produtos.models import *

admin.site.register(TipoProduto)
admin.site.register(TipoCozinha)
admin.site.register(Estabelecimento)
admin.site.register(Endereco)
admin.site.register(Contato)
admin.site.register(Horario)
admin.site.register(Produto)