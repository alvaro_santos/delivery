#-*- coding: utf-8 -*-
from django.db import models
from produtos.models import *
from django.template.defaultfilters import slugify

class Estabelecimento(models.Model):
	nome = models.CharField(max_length=50)
	logotipo = models.CharField(max_length=100)
	slug = models.SlugField(max_length=50)
	foto = models.CharField(max_length=255)
	capa = models.CharField(max_length=255)

	def __unicode__(self):
		return self.nome

	def save(self, *args, **kwargs):
		self.slug = slugify( self.nome )
		super(Estabelecimento, self).save(*args, **kwargs)

	def getProdutosByTipo(self, tipoProdutoPk):
		return self.produtos.filter(tipoProduto=tipoProdutoPk)
	
	def showEndereco(self):
		return self.enderecos.all()[0]

	def showContatos(self):
		contatos = ""
		for contato in self.contatos.all():
			contatos += u"%s, " % ( contato )
		return contatos

	def showCozinhas(self):
		cozinhas = ""
		for cozinha in self.tiposCozinha.all():
			cozinhas += u"%s, " % ( cozinha )
		return cozinhas

class Endereco(models.Model):
	rua = models.CharField(max_length=255)
	bairro = models.CharField(max_length=255)
	numero = models.CharField(max_length=255)
	complemento = models.CharField(max_length=255)
	longitude = models.CharField(max_length=255)
	latitude = models.CharField(max_length=255)
	estabelecimento = models.ForeignKey(Estabelecimento, related_name="enderecos")

	def __unicode__(self):
		return self.rua+", "+self.numero+", "+self.bairro

class Contato(models.Model):
	tipo = models.CharField(max_length=255)
	contato = models.CharField(max_length=255)
	estabelecimento = models.ForeignKey(Estabelecimento, related_name="contatos")
	
	def __unicode__(self):
		return u"%s" % ( self.contato )
		

class Horario(models.Model):
	dia = models.CharField(max_length=255)
	abertura = models.CharField(max_length=255)
	encerramento = models.CharField(max_length=255)
	estabelecimento = models.ForeignKey(Estabelecimento, related_name="horarios")
	def __unicode__(self):
		return u"%s de %s às %s" % ( self.dia, self.abertura, self.encerramento )

class TipoCozinha(models.Model):
	nome = models.CharField(max_length=255)
	slug = models.SlugField(max_length=100)
	stabelecimento = models.ManyToManyField(Estabelecimento, related_name="tiposCozinha")

	def __unicode__(self):
		return self.nome

	def save(self, *args, **kwargs):
		self.slug = slugify( self.nome )
		super(TipoCozinha, self).save(*args, **kwargs)