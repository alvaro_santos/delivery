#-*- coding: utf-8 -*-
from django.shortcuts import render_to_response, get_object_or_404
from .models import *
# Create your views here.
def index(request):
	estabelecimentos = Estabelecimento.objects.all()
	return render_to_response('estabelecimentos/index.html', locals())

def cozinha(request, tipo_cozinha):
	estabelecimentos = Estabelecimento.objects.filter(tiposCozinha__slug=tipo_cozinha)
	return render_to_response('estabelecimentos/index.html', locals())

def home(request, slug_estabelecimento):
	estabelecimento = get_object_or_404(Estabelecimento, slug=slug_estabelecimento)

	query = Estabelecimento.objects.filter(slug=slug_estabelecimento)
	tiposProdutos = estabelecimento.tiposProduto.all()
	produtos = list()
	for tpProduto in tiposProdutos:
		produtos.append( {
			"tipoProduto": tpProduto.nome,
			"produtos": estabelecimento.getProdutosByTipo( tpProduto.pk )
		})
	return render_to_response('estabelecimentos/home.html', locals())
