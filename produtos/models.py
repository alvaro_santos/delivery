#-*- coding: utf-8 -*-
from django.db import models
from django.template.defaultfilters import slugify

class TipoProduto(models.Model):
	nome = models.CharField(max_length=255)
	estabelecimento = models.ManyToManyField( 'estabelecimentos.Estabelecimento', related_name="tiposProduto", null=True,blank=True,)
	def __unicode__(self):
		return self.nome

class Produto(models.Model):
	nome = models.CharField(max_length=255)
	slug = models.SlugField(max_length=100)
	preco = models.CharField(max_length=255,blank=True,null=True,)
	foto = models.CharField(max_length=255,blank=True,null=True,)
	estabelecimento = models.ForeignKey('estabelecimentos.Estabelecimento',related_name="produtos", blank=True, null=True)
	tipoProduto = models.ForeignKey(TipoProduto)
	componentes = models.ManyToManyField( "self", related_name="composto",blank=True,  null=True)
	
	def __unicode__(self):
		return self.nome
	
	def save(self, *args, **kwargs):
		self.slug = slugify( self.nome )
		super(Produto, self).save(*args, **kwargs)

	def showComponentes(self):
		componentes = ""
		for componente in self.componentes.all():
			componentes += u"%s, " % ( componente )
		return componentes
		